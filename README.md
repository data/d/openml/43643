# OpenML dataset: Early-Stage-Diabetes-Risk-Prediction-Dataset

https://www.openml.org/d/43643

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Data Set Information:
This has been collected using direct questionnaires from the patients of Sylhet Diabetes
Hospital in Sylhet, Bangladesh and approved by a doctor.
Data Set Information:
This has been col-
lected using direct questionnaires from the patients of Sylhet Diabetes
Hospital in Sylhet, Bangladesh and approved by a doctor.
Attribute Information:
Age 1.20-65
Sex 1. Male, 2.Female
Polyuria 1.Yes, 2.No.
Polydipsia 1.Yes, 2.No.
sudden weight loss 1.Yes, 2.No.
weakness 1.Yes, 2.No.
Polyphagia 1.Yes, 2.No.
Genital thrush 1.Yes, 2.No.
visual blurring 1.Yes, 2.No.
Itching 1.Yes, 2.No.
Irritability 1.Yes, 2.No.
delayed healing 1.Yes, 2.No.
partial paresis 1.Yes, 2.No.
muscle stiness 1.Yes, 2.No.
Alopecia 1.Yes, 2.No.
Obesity 1.Yes, 2.No.
Class 1.Positive, 2.Negative.
Relevant Papers:
Likelihood Prediction of Diabetes at Early Stage Using Data Mining Techniques
[Web Link]
Authors and affiliations
M. M. Faniqul IslamEmail
Rahatara Ferdousi
Sadikur Rahman
Humayra Yasmin Bushra
Citation Request:
Islam, MM Faniqul, et al. 'Likelihood prediction of diabetes at early stage using data mining techniques.' Computer Vision and Machine Intelligence in Medical Image Analysis. Springer, Singapore, 2020. 113-125.
Islam, MM Faniqul, et al. 'Likelihood prediction of diabetes at early stage using data mining techniques.' Computer Vision and Machine Intelligence in Medical Image Analysis. Springer, Singapore, 2020. 113-125.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43643) of an [OpenML dataset](https://www.openml.org/d/43643). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43643/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43643/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43643/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

